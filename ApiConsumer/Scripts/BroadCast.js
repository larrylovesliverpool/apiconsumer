﻿

(function ($) {

    var HTTPstatusCode = {};

    HTTPstatusCode.Ok = 200;
    HTTPstatusCode.NotFound = 404;

    $.ApplicationAlert = function (options) {

        // Default settings
        var settings = $.extend({
            application: 0,
            url: "",
            key: "",
            text: 'Default Message, No message text.',
            bootstrap: false,
            complete: null,
            success: null,
            error: null,
            position: "top",
            width:"100"
        }, options);

        var Show = function () {

            var message = getMessageText(settings.application,settings.url,settings.key);

            if (message.status == HTTPstatusCode.Ok) {

                if (settings.position == "top" || settings.position == "Top") {
                    if (settings.bootstrap)
                        $('body').prepend(MessageBootstrap(message.text));
                    else
                        $('body').prepend(MessageStandard(message.text, settings.width));
                } else {
                    if (settings.bootstrap)
                        $('body').append(MessageBootstrap(message.text));
                    else
                        $('body').append(MessageStandard(message.text, settings.width));
                };
            }
        };

        var Hide = function () {
            alert("hide me now");
        };

        return { Show:Show, Hide:Hide }
    };

    $.fn.BroadCast = function (options) {

        // Default settings
        var settings = $.extend({
            application: 0,
            url: "",
            key: "",
            text: 'Default Message, No message text.',
            bootstrap: false,
            complete: null,
            success: null,
            error: null,
            position: "top",
            width: "100"
        }, options);

        return this.each(function () {

            var message = getMessageText(settings.application, settings.url, settings.key);

            if (message.status == HTTPstatusCode.Ok)
            {
                if (settings.bootstrap)
                    $(this).append(MessageBootstrap(message.text));
                else
                    $(this).append(MessageStandard(message.text, settings.width));
            }

            if ($.isFunction(settings.complete)) {
                settings.complete.call(this);
            }

            if ($.isFunction(settings.success)) {
                settings.success.call(this);
            }

            if ($.isFunction(settings.error)) {
                settings.position.call(this);
            }
        });

    }

    // Private functions
    function getMessageText(applicationId,url,key)
    {
        var message = {};

        var address = url + "?id=" + applicationId + "&key=" + key;

        $.ajax({
            url: address,
            dataType: 'json',
            async: false,
            success: function (result, textStatus, jqXHR) {
                message.status = jqXHR.status;
                message.textStatus = textStatus;
                message.text = result.text;
            },
            error: function (result, textStatus, jqXHR) {
                message.status = result.status
                message.textStatus = jqXHR;
                message.text = "";
            }
        });

        return message;
    }

    function MessageBootstrap(messagetext) {

        var HTMLmessage = '';

        HTMLmessage += '<div class="container" >';

        HTMLmessage += '<div id="BroadCast" class="row">';
        HTMLmessage += '<div class="alert alert-info" role="alert">';
        HTMLmessage += '<h4 class="alert-heading">System Message</h4>'
        HTMLmessage += '<p>' + messagetext + '</p>';
        HTMLmessage += '</div>';
        HTMLmessage += '</div>';

        HTMLmessage += '</div>';

        return HTMLmessage;
    }

    function MessageStandard(messagetext,width) {

        var HTMLmessage = '';

        HTMLmessage += '<div id="BroadCast" style="width:'+ width +'%; border:2px solid silver; background-color:lightgray; margin-left:auto; margin-right:auto; border-radius: 6px; padding:10px; padding-bottom:0px;" >';
        HTMLmessage += '<p>'
        HTMLmessage += '<strong>System Message</strong><br/>';
        HTMLmessage += messagetext;
        HTMLmessage += '</p></div>';

        return HTMLmessage;
    }

}(jQuery));


