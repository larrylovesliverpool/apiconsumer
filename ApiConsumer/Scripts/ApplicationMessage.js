﻿
/* *************** */
/* Author D.Morton */
/* *************** */

function DisplayMessage(systemIdentifier,position) {

    systemIdentifier = typeof systemIdentifier !== 'undefined' ? systemIdentifier : 0;
    position = typeof position !== 'undefined' ? position : 'top';

    $.getJSON('http://localhost:53480/api/messages/GetMessages/' + systemIdentifier, function (result) {

        /* a test for bootstrap, not comprehensive but a quick check */
       if( bootstrap_enabled = (typeof $().modal == 'function') )
            ShowMessageBootstrap(result.text,position);
       else
            ShowMessageHeader(result.text, position);
    });
}

function ShowMessageBootstrap(messagetext,position) {

    var HTMLmessage = '';

    HTMLmessage += '<div id="notification-bar">';
    HTMLmessage += '<div class="container">';
    HTMLmessage += '<div class="row">';
    HTMLmessage += '<div class="alert alert-info" role="alert">';
    HTMLmessage += '<h4 class="alert-heading">System Message</h4>'
    HTMLmessage += '<p>' + messagetext + '</p>';
    HTMLmessage += '</div>';
    HTMLmessage += '</div>';
    HTMLmessage += '</div>';
    HTMLmessage += '</div>';

    showMessageNotification(HTMLmessage, position);
}

function ShowMessageHeader(messagetext, position, bgcolor, txtcolor, height) {

    /* ************** */
    /* default values */
    /* ************** */

    bgColor = typeof bgColor !== 'undefined' ? bgColor : "#e6ecff";
    txtColor = typeof txtColor !== 'undefined' ? txtColor : "#99b3ff";
    height = typeof height !== 'undefined' ? height : 60;

    var HTMLmessage = "";

    HTMLmessage += '<div id="notification-message" style="text-align:left; text-indent:25px; line-height:' + height + 'px;" >';
    HTMLmessage += 'System Message: ';
    HTMLmessage += messagetext;
    HTMLmessage += '</div>';

    HTMLmessage = "<div id='notification-bar' style='display:inline; width:95%; height:" + height + "px; background-color: " + bgColor + "; position: fixed; z-index: 100; color: " + txtColor + ";border: 2px solid " + txtColor + ";border-radius: 6px; margin:5px;'>" + HTMLmessage + "</div>"

    showMessageNotification(HTMLmessage, position);
}

function showMessageNotification(HTMLmessage,position) {

    /* *************************************************** */
    /* Create the notification bar div if it doesn't exist */
    /* *************************************************** */

    if ($('#notification-bar').size() == 0) {

        if (position === 'bottom' || position === 'top' || position === 'Bottom' || position === 'Top') {
            if (position === 'bottom' || position === 'Bottom')
                $('body').append( HTMLmessage );
            else
                $('body').prepend( HTMLmessage );
        }
        else {
            /* code defined position */

            if ($('#' + position).length) {
                $('#' + position).append( HTMLmessage );
            }
        }
    }
}

/* EOF */