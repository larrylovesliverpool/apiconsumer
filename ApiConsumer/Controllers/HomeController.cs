﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ApiConsumer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult BootstrapHolder()
        {
            ViewBag.Message = "BootstrapHolder.";

            return View();
        }

        public ActionResult CssHolder()
        {
            ViewBag.Message = "CssHolder.";

            return View();
        }

        public ActionResult BootstrapTop()
        {
            ViewBag.Message = "BootstrapTop.";

            return View();
        }

        public ActionResult CssTop()
        {
            ViewBag.Message = "CssTop.";

            return View();
        }

        public ActionResult BootstrapBottom()
        {
            ViewBag.Message = "BootstrapBottom.";

            return View();
        }

        public ActionResult CssBottom()
        {
            ViewBag.Message = "CssTop.";

            return View();
        }

        public ActionResult HowTo()
        {
            ViewBag.Message = "How To";

            return View();
        }

    }
}