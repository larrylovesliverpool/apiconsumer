﻿

$.widget("SystemMessage.ApplicationMessage", {

    options: {
        // Default options
    },

    _create: function () {
        // Initialization logic here
    },

    // Create a public method.
    myPublicMethod: function (argument) {
        // ...
    },

    // Create a private method.
    _myPrivateMethod: function (argument) {
        // ...
    }

});